import XCTest
@testable import ComponentsX

final class ComponentsXTests: XCTestCase {
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct
        // results.
        XCTAssertEqual(ComponentsX().text, "Hello, World!")
    }
}
